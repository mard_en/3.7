curl --request POST --header "PRIVATE-TOKEN: $api_token" "https://gitlab.com/api/v4/projects/25931534/pipeline?ref=master" \
--form "variables[][key]=IMAGE_NGNX" --form "variables[][variable_type]=env_var" --form "variables[][value]=$IMAGE_NGNX" \
--form "variables[][key]=IMAGE_APP" --form "variables[][variable_type]=env_var" --form "variables[][value]=$IMAGE_APP" \
--form "variables[][key]=NGNX_PRT" --form "variables[][variable_type]=env_var" --form "variables[][value]=$NGNX_PRT" \
--form "variables[][key]=DOWN_ART" --form "variables[][variable_type]=env_var" --form "variables[][value]=$DOWN_ART"
DEPLOY_ID=$(curl -k --location --header "PRIVATE-TOKEN:$api_token" "https://gitlab.com/api/v4/projects/25931534/pipelines" | jq '.[] | .id' | head -1)
JOB_ID=$(curl -k --location --header "PRIVATE-TOKEN:$api_token" "https://gitlab.com/api/v4/projects/25931534/pipelines/$DEPLOY_ID/jobs" | jq '.[] | select(.name == "run_app") | .id')
curl -k --request POST --header "PRIVATE-TOKEN:$api_token" "https://gitlab.com/api/v4/projects/25931534/jobs/$JOB_ID/play"
